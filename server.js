const app = require('./app');
const port = process.env.PORT || 5000;

/** Run Server */
app.listen(port, function() {
  console.log("Server is running on Port: " + port);
});
/** Run Server */