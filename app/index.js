const express = require("express");
const nocache = require('nocache');
const app = express();
const bodyParser = require('body-parser');

const welcome = require("./routes/welcome");

/** Funciones adicionales*/
const cors = require('cors')
app.use(cors())

app.use(nocache());

/**
 * CORS: Cross-origin resource sharing.
 * Is a browser security feature that restricts cross-origin HTTP requests 
 * that are initiated from scripts running in the browser.
 */
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});

/** Decode Form URL Encoded data */
app.use(bodyParser.json())

/**Endpoints */
app.use('/welcome', welcome);

module.exports = app

